import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/pages/first.view.dart';
import 'package:getx/pages/second.view.dart';

void main() {
  runApp(const MyApp());
}
//getx context base nist yano faghat onjaii ke migi ro up mikone va vighegi haye bishtari ham nesbat be redux
//va provider dare ke khili mothmer thamare
//focus getx ro optimize kardan ram computeremone
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      initialRoute: '/first',
      getPages: [
        GetPage(name: '/first', page: ()=> const First()),
        GetPage(name: '/second', page: ()=>  Second()),
      ],
    );
  }
}
