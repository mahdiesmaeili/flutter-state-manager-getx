import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/controller/increament.get.dart';

class Second extends StatelessWidget {
  Second({Key? key}) : super(key: key);
//inja instantiate karde va in instantiate monjar be in shode ke az class first ham
  //eghdam koni
  final incrementController = Get.put(IncrementController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second page"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Obx(()=>
                Text('Number is : ${incrementController.currentNumber.value}',
                  style: Theme.of(context).textTheme.bodyText1,)
            ),
            TextButton(onPressed: ()  => incrementController.increment() ,
                child: Text('Action',style: Theme.of(context).textTheme.bodyText1,))
          ],
        ),
      ),
    );
  }
}
