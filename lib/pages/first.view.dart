import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class First extends StatelessWidget {
  const First({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("First page"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextButton(
                onPressed: () {
                  Get.toNamed('/second');
                },
                child: Text(
                  'Go to Scond page toNamed',
                  style: Theme.of(context).textTheme.bodyText1,
                )),
            //off named ro estefade koni miad replace mikone button back ro ham hazf mikone
            TextButton(
                onPressed: () {
                  Get.offNamed('/second');
                },
                child: Text(
                  'Go to Scond page offNamed',
                  style: Theme.of(context).textTheme.bodyText1,
                )),
            //off all named ro estefade koni miad replace mikone button back ro ham hazf mikone be enzemam tamami
            //safahat ghabli off named yeki ghabli ro hazf mikon off all named koliye safehaye dige ro hazf mikone
            TextButton(
                onPressed: () {
                  Get.offAllNamed('/second');
                },
                child: Text(
                  'Go to Scond page offAllNamed',
                  style: Theme.of(context).textTheme.bodyText1,
                )),
          ],
        ),
      ),
    );
  }
}
